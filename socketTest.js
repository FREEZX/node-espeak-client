var io = require('socket.io').listen(320);
var sys = require('sys');

io.sockets.on('connection', function (socket) {
    socket.on('message', function (data) {
        console.log(data);
        var args = '';
        if(data.lang){
            args+="-v "+data.lang+" ";
        }
        if(data.speed){
            args+="-s "+data.speed+" ";
        }
        if(data.pitch){
            args+="-p "+data.pitch+" ";
        }
        var command = 'espeak '+args+' "'+data.msg+'"';
        sys.exec(command, function(){
            console.log('speak success');
            socket.emit('playSuccess');
        });
    });
});